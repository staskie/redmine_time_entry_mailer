require 'mail_handler'

module RedmineTimeEntryMailer
  module MailHandlerPatch

    def self.included(base)
      unloadable

      base.class_eval do
        alias_method :org_receive_issue, :receive_issue
        alias_method :org_receive_issue_reply, :receive_issue_reply

        def receive_issue
          issue = org_receive_issue
          manage_time_entry(issue)
          issue.update_attributes(description: cleaned_up_text_body) if issue.present?
          issue
        end

        def receive_issue_reply(one, two = nil)
          journal = org_receive_issue_reply(one, two)
          manage_time_entry(journal.issue)

          journal.update_attributes(notes: cleaned_up_text_body) if journal.present?
          journal
        end

        private :org_receive_issue
        private :org_receive_issue_reply
        private :receive_issue
        private :receive_issue_reply
      end

      def manage_time_entry(issue)
        attrs = {
          'timeid' => get_keyword(:timeid, {override: true}),
          'timeissue' =>  get_keyword(:timeissue, {override: true}),
          'timedate' => get_keyword(:timedate, {override: true}),
          'timehours' => get_keyword(:timehours, {override: true}),
          'timecomment' => get_keyword(:timecomment, {override: true}),
          'timeactivity' => get_keyword(:timeactivity, {override: true}),
          'project' => get_keyword(:project)
        }.delete_if {|k, v| v.blank? }

        # User doesn't want to create an issue
        return if attrs['timehours'].blank?

        # timeid
        if attrs['timeid'].blank?
          time_entry = TimeEntry.new
        else
          time_entry = TimeEntry.where(id: attrs['timeid']).first
          # User provided invalid time entry - don't create
          return if time_entry.blank?
        end

        # user_id
        if user
          time_entry.user_id = user.id
        end

        # timeissue - ugh
        issue_id = attrs['timeissue']
        if issue_id.present?
          # Fetch issue from `timeissue`
          time_issue = Issue.where(id: issue_id).first
          if time_issue
            time_entry.issue_id = time_issue.id
            time_entry.project_id = time_issue.project_id
          else
            project_name = attrs['project']
            if prject_name.present?
              time_entry.project_id = Project.where(name: project_name).first.try(:id)
            else
              return # can't create a time entry without a project
            end
          end
        elsif issue.present?
          # Use an issue passed as an argument
          time_entry.issue_id = issue.id
          time_entry.project_id = issue.project_id
        else
          # no issue, create a ticket on a project only
          project_name = attrs['project']
          if prject_name.present?
            time_entry.project_id = Project.where(name: project_name).first.try(:id)
          else
            return # can't create a time entry without a project
          end
        end

        # timedate
        if attrs['timedate'].present?
          begin
            date = Date.parse(attrs['timedate'])
          rescue
            date = Date.today
          end
        else
          date = Date.today
        end

        time_entry.spent_on = date

        # timehours
        if attrs['timehours'].present?
          time_entry.hours = attrs['timehours']
        end

        settings = Setting['plugin_redmine_time_entry_mailer']

        # timecomment
        if attrs['timecomment'].present?
          time_entry.comments = attrs['timecomment']
        else
          time_entry.comments = settings['timecomment']
        end

        # timeactivity
        if attrs['timeactivity'].present?
          time_entry.activity_id = TimeEntryActivity.where('lower(name) = ?', attrs['timeactivity'].downcase).first.try(:id)
        else
          time_entry.activity_id = settings['timeactivity']
        end

        # ensure we have a default time activity if finding by name goes wrong
        if time_entry.activity_id.nil?
          time_entry.activity_id = TimeEntryActivity.active.first.id
        end

        time_entry.save!
        time_entry
      end
    end
  end
end

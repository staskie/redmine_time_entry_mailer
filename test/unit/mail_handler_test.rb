# encoding: utf-8
#
# Redmine - project management software
# Copyright (C) 2006-2015  Jean-Philippe Lang
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

require File.expand_path('../../test_helper', __FILE__)

class MailHandlerTest < ActiveSupport::TestCase
  fixtures :users, :projects, :enabled_modules, :roles,
           :members, :member_roles, :users,
           :email_addresses,
           :issues, :issue_statuses,
           :workflows, :trackers, :projects_trackers,
           :versions, :enumerations, :issue_categories,
           :custom_fields, :custom_fields_trackers, :custom_fields_projects,
           :boards, :messages

  FIXTURES_PATH = File.dirname(__FILE__) + '/../fixtures/mail_handler'

  def setup
    ActionMailer::Base.deliveries.clear
    Setting.notified_events = Redmine::Notifiable.all.collect(&:name)
  end

  def teardown
    Setting.clear_cache
  end

  def test_adding_issue_with_time_entry
    ActionMailer::Base.deliveries.clear
    assert_equal TimeEntry.count, 0
    issue = submit_email('adding_issue_with_time_entry.eml')
    issue.reload
    time_entry = TimeEntry.first

    assert_equal time_entry.project, issue.project
    assert_equal time_entry.issue, issue
    assert_equal time_entry.hours, 3
    assert_equal time_entry.spent_on, Date.today

    # Email notification should be sent
    mail = ActionMailer::Base.deliveries.last
    assert_not_nil mail
    assert mail.subject.include?("##{issue.id}")
    assert mail.subject.include?('New ticket on a given project')
  end

  def test_updating_existing_time_entry
    old_time_entry =TimeEntry.create!(
      project: Project.first,
      user: User.first,
      hours: 1,
      spent_on: Date.today,
      activity_id: TimeEntryActivity.active.first.id)
    old_time_entry.update_attribute(:id, 1)

    ActionMailer::Base.deliveries.clear
    journal = submit_email('updating_existing_time_entry.eml')
    issue = journal.issue
    issue.reload
    time_entry = TimeEntry.first

    assert_equal time_entry.project, issue.project
    assert_equal time_entry.issue, issue
    assert_equal time_entry.hours, 6
    assert_equal time_entry.spent_on, Date.parse('2015-01-01')

    # Email notification should be sent
    mail = ActionMailer::Base.deliveries.last
    assert_not_nil mail
    assert mail.subject.include?("##{issue.id}")
  end

  def test_updating_exsiting_issue_with_time_entry_use_case_1
    ActionMailer::Base.deliveries.clear
    assert_equal TimeEntry.count, 0
    journal = submit_email('updating_existing_issue_with_time_entry_use_case_1.eml')
    issue = journal.issue.reload
    time_entry = TimeEntry.first

    assert_equal time_entry.project, issue.project
    assert_equal time_entry.issue, issue
    assert_equal time_entry.hours, 1.75
    assert_equal time_entry.spent_on, Date.parse('2018-11-28')
    assert_equal time_entry.comments, 'Time comments to see if this works ok'

    # Email notification should be sent
    mail = ActionMailer::Base.deliveries.last
    assert_not_nil mail
    assert mail.subject.include?("##{issue.id}")
  end

  def test_updating_exsiting_issue_with_time_entry
    ActionMailer::Base.deliveries.clear
    assert_equal TimeEntry.count, 0
    journal = submit_email('updating_existing_issue_with_time_entry.eml')
    issue = journal.issue.reload
    time_entry = TimeEntry.first

    assert_equal time_entry.project, issue.project
    assert_equal time_entry.issue, issue
    assert_equal time_entry.hours, 8
    assert_equal time_entry.spent_on, Date.today

    # Email notification should be sent
    mail = ActionMailer::Base.deliveries.last
    assert_not_nil mail
    assert mail.subject.include?("##{issue.id}")
  end

  def test_adding_time_entry_without_issue
    ActionMailer::Base.deliveries.clear
    assert_equal TimeEntry.count, 0
    issue = submit_email('adding_time_entry_without_issue.eml')
    issue.reload
    time_entry = TimeEntry.first

    assert_equal time_entry.project, issue.project
    assert_equal time_entry.issue, issue
    assert_equal time_entry.hours, 3
    assert_equal time_entry.spent_on, Date.today

    # Email notification should be sent
    mail = ActionMailer::Base.deliveries.last
    assert_not_nil mail
    assert mail.subject.include?("##{issue.id}")
    assert mail.subject.include?('New ticket on a given project')
  end

  private

  def submit_email(filename, options={})
    raw = IO.read(File.join(FIXTURES_PATH, filename))
    yield raw if block_given?
    MailHandler.receive(raw, options)
  end

  def assert_issue_created(issue)
    assert issue.is_a?(Issue)
    assert !issue.new_record?
    issue.reload
  end
end

require 'redmine'

Redmine::Plugin.register :redmine_time_entry_mailer do
  name 'Redmine Time Entry Mailer plugin'
  author 'Dominik Staskiewicz'
  description 'This plugin creates time entries from inbound email'
  version '0.0.1'
  url 'http://dapplication.com'
  author_url 'http://dapplication.com'


  requires_redmine :version_or_higher => '3.0.0'

  settings default: {},
           partial: 'settings/invoices'

  require_relative 'lib/redmine_time_entry_mailer/mail_handler_patch'

  MailHandler.send(:include, RedmineTimeEntryMailer::MailHandlerPatch)
end
